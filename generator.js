paper.install(window);

//do not expose these
let COLOR_PALETTE;
let BG_COLOR;
let SEGMENTS = 6;
let ANGLE = 360 / SEGMENTS;
let CENTER;
let symbols = [];
let hexaCopy;

//could be exposed but maybe we should set them
let genSize = 200;
let space = 10;
let backgroundOpacity = 1; //0.3;

//expose these for users to play with :)
let fillBackground = true;
let monochrome = false;
let text = 'May Contain Hackers';


window.onload = function() {
	paper.setup('paperCanvas');
    CENTER = view.bounds.center;
    //main generation function
    generateHexagon();
}


function generateHexagon(){
    clearCanvas();
    let triangle = new Path();
    let sizeVec = new Point(0, genSize);

    triangle.addSegment(CENTER);
    triangle.addSegment(CENTER.add( sizeVec.rotate(ANGLE/2) ));
    triangle.addSegment(CENTER.add( sizeVec.rotate(-ANGLE/2) ));
    triangle = roundCorners(triangle, 40);
    let triangleCenter = triangle.position;

    let shapes1 = genShapes(triangle);

    let grp;
    if(monochrome){
        grp = new Group();

        shapes1.forEach(shape => {
            let tmp = triangle.subtract(shape);
            triangle.remove();
            shape.remove();
            triangle = tmp;
        });
        triangle.fillColor = BG_COLOR;
        grp.addChild(triangle);
    }else{
        let tBG = triangle.clone();
        tBG.fillColor = BG_COLOR;
        COLOR_PALETTE.shift();
        let shapes2 = genShapes(triangle);
        grp = new Group({children: [triangle, tBG].concat(shapes1).concat(shapes2)});
        grp.clipped = true;
        COLOR_PALETTE.shift();
    }
    grp.translate([0,space]);

    let mainHexagon = spread(grp, triangleCenter);
    hexaCopy = mainHexagon.clone();
    hexaCopy.position = [-genSize*3, -genSize*3];
    if(fillBackground){
        mainHexagon = fillView(mainHexagon);
        setText(mainHexagon, text);
    }

}

function clearCanvas(){
    project.activeLayer.removeChildren();
    COLOR_PALETTE = shuffle(['#fa448c', '#fec859', '#43b5a0', '#491d88', '#331a38']);
    BG_COLOR = COLOR_PALETTE.slice(-1)[0];
    symbols = [];
    hexaCopy = null;
}

function toggleMonochrome(){
    monochrome = !monochrome;
    generateHexagon();
}

function toggleFill(){
    fillBackground = !fillBackground;
    generateHexagon();
}

function setText(mainHexagon, text){
    let removeLater = [];
    opentype.load("Alice-Regular.ttf", function(err, font) {

        if (err) {
            console.log(err.toString());
            return;
        }
        let fontPath = font.getPath(text,0,0,genSize*0.8);
        let textPath = paper.project.importSVG(fontPath.toSVG());
        textPath.fillColor = monochrome==true? BG_COLOR : COLOR_PALETTE[0];
        textPath.position.y = mainHexagon.position.y - (genSize-space)/2;
        textPath.bounds.topLeft.x = 50+ Math.random() * (project.view.bounds.width - textPath.bounds.width - 100);
        symbols.forEach(symbol => {
            if(symbol.intersects(textPath)){
                let newCopy = hexaCopy.clone();
                newCopy.position = symbol.position;
                newCopy.children.forEach(tri => {
                    if(tri.children[0].intersects(textPath)){
                        removeLater.push(tri);
                    }
                })
                symbol.remove();
            }
        });
        removeLater.forEach(tri => tri.tweenTo({
            opacity: 0,
            position: [tri.position.x,1000],
            rotation: Math.random()*20 - 10
        }, {duration: Math.random()*1000 + 500}));
        hexaCopy.remove();
        //textPath.scale(1.2);
        textPath.rotate(Math.random()*10-5);
        textPath.sendToBack();
    });
}

function fillView(hexagon){
    let offset = Point.random().multiply([-genSize, -genSize]);
    let symbol = new Symbol(hexagon);
    let hexaWidthDist = genSize*3 + space*5
    let hexaHeightDist = (hexagon.bounds.height + space)/2
    let lineCount = 0;

    for(let y = offset.y; y<project.view.bounds.height+hexaHeightDist; y+=hexaHeightDist){
        for(let x = offset.x; x<project.view.bounds.width+hexaWidthDist; x+=hexaWidthDist){
            if(lineCount%2 == 0){
                symbols.push(symbol.place([x,y]));
            }else{
                symbols.push(symbol.place([x+hexaWidthDist/2,y]));
            }
        }
        lineCount++;
    }
    symbols.forEach(sym => sym.opacity = backgroundOpacity);
    let mainSymbol = symbols[Math.floor(Math.random() * symbols.length)];
    while(!project.view.bounds.contains(mainSymbol.bounds)){
        mainSymbol = symbols[Math.floor(Math.random() * symbols.length)];
    }
    mainSymbol.opacity = 1;
    return mainSymbol;
}

function genShapes(path){
    let shapes = [];
    let timeout = 5;
    let counter = 0
    while(counter < timeout){
        let newLine = setLine(path);
        while(checkIntersection(shapes, newLine) && counter < timeout){
            newLine.remove();
            newLine = setLine(path);
            counter++;
        }
        if(counter < timeout){
            counter = 0;
            shapes.push(newLine);
        }else{
            newLine.remove();
        }
    }
    return shapes;
}

function checkIntersection(arr, path){
    return arr.filter(item => item.intersects(path)).length > 0
}

function getRndPointOnPath(path){
    return path.getPointAt(path.length * Math.random());
}

function setLine(path){
    let trace = new Path.Rectangle([0, 0], [genSize/15, 40]);

    let pL = trace.bounds.topLeft;
    let pR = trace.bounds.topRight;
    trace = placeVia(trace, trace.bounds.bottomCenter);

    for(let i = 0; i<Math.random()*4+1; i++){
        [trace, pL, pR] = addLineSegment(trace, pL, pR);
    }
    trace = placeVia(trace, pL.add(pR.subtract(pL).divide(2)));

    trace.position = getRndPointOnPath(path);
    trace.rotate( (Math.floor(Math.random()*4)) *90);
    trace.fillColor = COLOR_PALETTE[0];
    return trace;
}

function addLineSegment(path, posLeft, posRight){
    let dir = Math.floor(Math.random()*4);
    let trace = new Path.Rectangle([0, 0], [genSize/15, 40]);

    trace.bounds.bottomLeft = posLeft;
    let pL = trace.bounds.topLeft;
    let pR = trace.bounds.topRight;

    switch(dir){
        case 0:
            path.rotate(45, posRight);
            break;
        case 1:
            path.translate([0, -10]);
            break;
        case 2:
            path.translate([0, -10]);
            break;
        case 3:
            path.rotate(-45, posLeft);
            break;
    }
    let tmp = trace.unite(path);
    trace.remove();
    path.remove();
    return [tmp, pL, pR];
}

function placeVia(path, pos){
    let type = Math.random();
    if(type < 0.3){
        let via = new Path.Circle(pos, genSize/15);
        let tmp = path.unite(via);
        via.remove();
        path.remove();
        return tmp;
    }else{
        let via = new Path.Circle(pos, genSize/15 * 1.5);
        let tmp = path.unite(via);
        via.remove();
        path.remove();
        path = tmp;
        via = new Path.Circle(pos, genSize/15 * 0.5);
        tmp = path.subtract(via);
        via.remove();
        path.remove();
        return tmp;
    }
}

function spread(grp, triangleCenter){
    let hexagon = new Group();
    hexagon.addChild(grp);
    for(let i = 1; i<SEGMENTS; i++){
        let newGrp = grp.clone();
        if(i%2==1){
            newGrp.scale(-1, 1, triangleCenter);
        }
        newGrp.rotate(ANGLE*i, CENTER);
        hexagon.addChild(newGrp);
    }
    return hexagon;
}

function roundCorners(path,radius) {
	var segments = path.segments.slice(0);
	path.removeSegments();

	for(var i = 0, l = segments.length; i < l; i++) {
		var curPoint = segments[i].point;
		var nextPoint = segments[i + 1 == l ? 0 : i + 1].point;
		var prevPoint = segments[i - 1 < 0 ? segments.length - 1 : i - 1].point;
		var nextDelta = curPoint.subtract(nextPoint);
		var prevDelta = curPoint.subtract(prevPoint);

		nextDelta.length = radius;
		prevDelta.length = radius;

		path.add(
			new paper.Segment(
				curPoint.subtract(prevDelta),
				null,
				prevDelta.divide(2)
			)
		);

		path.add(
			new paper.Segment(
				curPoint.subtract(nextDelta),
				nextDelta.divide(2),
				null
			)
		);
	}
	path.closed = true;
	return path;
}

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}
